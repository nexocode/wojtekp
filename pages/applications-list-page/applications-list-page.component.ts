import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSelectChange, MatTabChangeEvent, MatTabGroup } from '@angular/material';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { includes, isString, orderBy, uniq } from 'lodash';

import * as fromStore from '../../store';
import { Store } from '@ngrx/store';

import { FiltersAbstract } from 'abstracts/filters.abstract';
import { Applications } from 'resources/applications/applications';
import { ApplicationsFilterService } from 'services/applications-filter/applications-filter.service';
import {
  categoryFilterKey,
  orderFilterKey,
  regionFilterKey,
  searchQueryFilterKey,
  statusFilterKey
} from 'configs/paths.consts';
import { fade, fadeIn } from 'animations/fade.animations';
import { TitleService } from 'services/title/title.service';
import { UrlStateService } from 'services/url-state/url-state.service';
import { User } from 'resources/users/user.model';
import { applicationStatusFilters } from 'resources/applications/applications.consts';
import { Categories } from 'resources/categories/categories';
import { Regions } from 'resources/regions/regions';
import { OrderApps } from './order-applications.consts';

@Component({
  selector: 'applications-list-page',
  templateUrl: './applications-list-page.component.html',
  styleUrls: ['./applications-list-page.component.scss'],
  animations: [fade]
})
export class ApplicationsListPageComponent extends FiltersAbstract implements OnInit, OnDestroy {
  subscriptions: Subscription = new Subscription();

  @ViewChild('tabGroup') tabGroup: MatTabGroup;

  orders = [
    { value: OrderApps.ATOZ, viewValue: 'A to Z' },
    { value: OrderApps.YOUNGEST, viewValue: 'Newest Applications' },
    { value: OrderApps.OLDEST, viewValue: 'Oldest Applications' },
    { value: OrderApps.MOST_ADVANCED, viewValue: 'Most Advanced' },
    { value: OrderApps.LEAST_ADVANCED, viewValue: 'Least Advanced' },
  ];
  defaultOrder = this.orders[0].value;

  order: OrderApps;

  // selected status filter
  activeStatusFilter: Applications.Status = 0;

  // categories options to select
  categories: Categories.FlatList = [];

  // countries options to select
  regions: Regions.FlatList = [];

  categoriesList$: Observable<Categories.FlatList>;
  regionsList$: Observable<Regions.FlatList>;

  // displaied applications
  filteredApplications: Applications.List = [];

  pendingNumber: number;

  overdueNumber: number;

  fade: string;

  applications: Applications.List = [];

  private currentUser: User;

  private cachedApplications: Applications.List = [];

  private cachedRegions: Regions.FlatList = [];

  private cachedCategories: Categories.FlatList = [];

  constructor(
    private applicationsFilterService: ApplicationsFilterService,
    private store: Store<fromStore.HomeState>,
    protected urlStateService: UrlStateService
  ) {
    super(urlStateService);
    this.regionsList$ = this.store.select(fromStore.getRegionsFlatList).pipe(take(1));
    this.categoriesList$ = this.store.select(fromStore.getCategoriesFlatList).pipe(take(1));
    this.subscriptions.add(this.store.select(fromStore.getCurrentUser).subscribe(user => this.currentUser = new User(user)));
  }

  ngOnInit() {
    this.getApplications();
  }

  ngOnDestroy(){
    this.subscriptions.unsubscribe();
  }

  /**
   * Handle sorting applications by selected option
   * @param {MatSelectChange} selectedOption
   */
  onOptionSelect(selectedOption: MatSelectChange): void {
    this.order = selectedOption.value || OrderApps.ATOZ;
    this.sortFilteredApplications(this.order);
    this.urlStateService.navigate(orderFilterKey, this.order.toString());
  }

  /**
   * Handle switch tab event
   * @param {MatTabChangeEvent} $event
   * @listens (selectChange)
   */
  onStatusChange($event: MatTabChangeEvent = { index: this.activeStatusFilter } as MatTabChangeEvent): void {
    this.setStatusFilter($event.index);
    this.urlStateService.navigate(statusFilterKey, applicationStatusFilters[this.activeStatusFilter]);
  }

  /**
   * Retrieve applications from database then sort and set additional required params
   * extract categories and countries from applications and apply initial filters
   * @listens ngOnInit
   */
  private getApplications() {
    this.subscriptions.add(this.store.select(fromStore.getAllApplications).subscribe(applications => {
      this.applications = applications;
      this.filteredApplications = this.applicationsFilterService.filterByStatus(this.applications, this.activeStatusFilter);
      this.cachedApplications = [...this.filteredApplications];
      this.extractCategoriesAndCountries();
      this.updateFilters();
      this.fade = fadeIn;
    }));
  }

  /**
   * Extract categories and countries from applications
   * Then set filters based on user role categories and regions
   *
   * @listens getApplications
   */
  private extractCategoriesAndCountries(): void {
    this.categoriesToFilter = [];
    this.regionsToFilter = [];
    const categoriesToExtract = this.applications
      .filter(application => application.category)
      .map(application => application.category);

    const countriesToExtract = this.applications
      .filter(application => application.region)
      .map(application => application.region);

    this.categories = uniq(categoriesToExtract);
    this.regions = uniq(countriesToExtract);
    this.cachedCategories = [...this.categories];
    this.cachedRegions = [...this.regions];

    if (this.currentUser.isAdmin) {
      return;
    }

    if (this.currentUser.isRegionalLead) {
      this.regionsToFilter = this.regions.filter(region => includes(this.currentUser.roles.regionalLead.regions, region));
      this.urlStateService.navigate(regionFilterKey, this.regionsToFilter);

    } else {
      if (this.currentUser.isPoC) {
        this.categoriesToFilter.push(...this.categories
          .filter(category => includes(this.currentUser.roles.poc.categories, category)));
      }
      if (this.currentUser.isCommunityInterviewer) {
        this.categoriesToFilter.push(...this.categories
          .filter(category => includes(this.currentUser.roles.communityInterviewer.categories, category)));
      }
      if (this.currentUser.isProductInterviewer) {
        this.categoriesToFilter.push(...this.categories
          .filter(category => includes(this.currentUser.roles.productInterviewer.categories, category)));
      }
      this.categoriesToFilter = uniq(this.categoriesToFilter);
      this.urlStateService.navigate(categoryFilterKey, this.categoriesToFilter);
    }
  }

  /**
   * Fetch query params from url, update filters objects and call filter actions
   * Triggered once init view
   * @listens getApplications
   */
  private updateFilters(): void {
    let categoriesInUrl = this.urlStateService.getQueryParams(categoryFilterKey);
    let countriesInUrl = this.urlStateService.getQueryParams(regionFilterKey);
    const statusInUrl = this.urlStateService.getQueryParams(statusFilterKey) as string;
    const searchQueryInUrl = this.urlStateService.getQueryParams(searchQueryFilterKey) as string;
    const orderInUrl = this.urlStateService.getQueryParams(orderFilterKey) as OrderApps;

    if (categoriesInUrl) {
      categoriesInUrl = isString(categoriesInUrl) ? [categoriesInUrl] : categoriesInUrl;
      this.filterByCategories(categoriesInUrl);
    }
    if (countriesInUrl) {
      countriesInUrl = isString(countriesInUrl) ? [countriesInUrl] : countriesInUrl;
      this.filterByRegions(countriesInUrl);
    }
    if (statusInUrl) {
      const index = applicationStatusFilters.indexOf(statusInUrl);
      this.tabGroup.selectedIndex = index;
      this.setStatusFilter(index);
    }
    if (searchQueryInUrl) {
      this.searchByQuery(searchQueryInUrl);
    }
    if (orderInUrl) {
      this.order = orderInUrl;
      this.defaultOrder = orderInUrl;
    }

    this.applyFilters();
  }

  /**
   * Filter applications by given filters then count process properties.
   *
   * @listens updateFilters
   * @see ApplicationsFilterService.filterByProperties
   */
  protected applyFilters(): void {
    if (!this.regionsToFilter.length && !this.categoriesToFilter.length && !this.searchQuery) {
      this.filteredApplications = this.cachedApplications;
      this.countProcessProperties(this.applications);
    } else {
      const filterObject: Applications.Filters = {
        categories: this.categoriesToFilter,
        countries: this.regionsToFilter,
        searchQuery: this.searchQuery
      };
      this.filteredApplications = this.applicationsFilterService.filterByProperties(this.cachedApplications, filterObject);
      this.countProcessProperties(this.applicationsFilterService.filterByProperties(this.applications, filterObject));
    }
    this.sortFilteredApplications(this.order || OrderApps.ATOZ);
  }

  private sortFilteredApplications(order: OrderApps): void {
    if (order === OrderApps.ATOZ) {
      this.filteredApplications = orderBy(this.filteredApplications, [app => app.lastName.toLowerCase(), app => app.firstName.toLowerCase()]);
    }
    if (order === OrderApps.YOUNGEST) {
      this.filteredApplications = orderBy(this.filteredApplications, ['dateSubmitted', 'closed'], ['desc', 'asc']);
    }
    if (order === OrderApps.OLDEST) {
      this.filteredApplications = orderBy(this.filteredApplications, ['dateSubmitted', 'closed'], ['asc', 'asc']);
    }
    if (order === OrderApps.MOST_ADVANCED) {
      this.filteredApplications = orderBy(this.filteredApplications, ['processesState.step', 'closed'], ['desc', 'desc']);
    }
    if (order === OrderApps.LEAST_ADVANCED) {
      this.filteredApplications = orderBy(this.filteredApplications, ['processesState.step', 'closed'], ['asc', 'asc']);
    }
  }

  /**
   * Set active status filter and filter applications by given status
   * @param index
   * @listens onStatusChange
   * @listens updateFilters
   * @see ApplicationsFilterService.filterByStatus
   */
  private setStatusFilter(index): void {
    this.activeStatusFilter = index;
    this.filteredApplications = this.applicationsFilterService.filterByStatus(this.applications, this.activeStatusFilter);
    this.cachedApplications = [...this.filteredApplications];
    this.applyFilters();
  }

  /**
   * Count pending actions and overdue applications from given filtered applications
   * Applications can not be filtered by status
   * @param {Applications.List} applications
   * @listens applyFilters
   */
  private countProcessProperties(applications: Applications.List): void {
    const numbers = this.applicationsFilterService.countApplicationsProperties(applications);
    this.pendingNumber = numbers.pendingActions;
    this.overdueNumber = numbers.overdue;
  }
}
