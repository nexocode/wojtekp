export enum OrderApps {
  YOUNGEST = 'youngest',
  OLDEST = 'oldest',
  MOST_ADVANCED = 'most-adv',
  LEAST_ADVANCED = 'least-adv',
  ATOZ = 'atoz'
}
