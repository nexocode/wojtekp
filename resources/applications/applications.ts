import { Users } from 'resources/users/users';

export namespace Applications {

  export interface ReviewStatus {
    user: Users.Item;
    capacity?: boolean;
    eligibility: boolean;
    note: string;
    questions?: string;
  }

  export interface Feedback {
    lorem?: ReviewStatus;
    ipsum?: ReviewStatus;
    dolor?: ReviewStatus;
  }

  export interface Item {
    id: number;
    firstName: string;
    lastName: string;
    feedback: Feedback;
    category: string;
    region: string;
    dateSubmitted: number;
    closureDate?: number;
  }

  export type List = Item[];

  export enum Status {
    Active,
    All,
    Pending,
    Overdue,
    Approved,
    Closed
  }

  export interface Filters {
    categories?: string[];
    countries?: string[];
    searchQuery?: string;
  }

  export interface ProcessPropertiesNumbers {
    pendingActions: number,
    overdue: number
  }

  export interface Dictionary {
    [key: string]: List;
  }

  export interface Entities {
    [id: string]: Item
  }
}


