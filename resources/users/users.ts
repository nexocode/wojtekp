import { UserInfo as FirebaseUser } from 'firebase';
import { RoleName, Roles } from 'resources/roles/roles';
import { Applications } from 'resources/applications/applications';

export namespace Users {

  export interface Item extends Partial<FirebaseUser> {
    id?: string;
    firstName: string;
    lastName: string;
    roles: Roles;

    region?: string;
    hasSubmittedApplication?: boolean;
    application?: Applications.Item;
  }

  export type List = Item[];

  export interface Entities {
    [id: string]: Item
  }

}
