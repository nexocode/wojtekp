import { Action } from '@ngrx/store';

export const LOAD_APPLICATIONS = '[Applications] Load Applications';
export const LOAD_APPLICATIONS_FAIL = '[Applications] Load Applications Fail';
export const LOAD_APPLICATIONS_SUCCESS = '[Applications] Load Applications Success';

export class LoadApplications implements Action {
  readonly type = LOAD_APPLICATIONS;
}

export class LoadApplicationsFail implements Action {
  readonly type = LOAD_APPLICATIONS_FAIL;

  constructor(public payload: any) {}
}

export class LoadApplicationsSuccess implements Action {
  readonly type = LOAD_APPLICATIONS_SUCCESS;

  constructor(public payload: any) {}
}

// Action types
export type ApplicationsAction = LoadApplications | LoadApplicationsFail | LoadApplicationsSuccess;
