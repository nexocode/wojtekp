import { Action } from '@ngrx/store';

export const LOAD_REGIONS = '[Regions] Load Regions';
export const LOAD_REGIONS_FAIL = '[Regions] Load Regions Fail';
export const LOAD_REGIONS_SUCCESS = '[Regions] Load Regions Success';

export class LoadRegions implements Action {
  readonly type = LOAD_REGIONS;
}

export class LoadRegionsFail implements Action {
  readonly type = LOAD_REGIONS_FAIL;

  constructor(public payload: any) {}
}

export class LoadRegionsSuccess implements Action {
  readonly type = LOAD_REGIONS_SUCCESS;

  constructor(public payload: any) {}
}

// Action types
export type RegionsAction = LoadRegions | LoadRegionsFail | LoadRegionsSuccess;
