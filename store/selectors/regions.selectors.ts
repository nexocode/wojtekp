import { createSelector } from '@ngrx/store';

import * as fromRoot from '../../../store/index';
import * as fromFeature from '../reducers/index';
import * as fromRegions from '../reducers/regions.reducer';

import { regionIdParam } from 'configs/paths.consts';

export const getRegionsState = createSelector(
  fromFeature.getHomeState,
  (state: fromFeature.HomeState) => state.regions
);

export const getRegionsList = createSelector(getRegionsState, fromRegions.getRegionsList);
export const getRegionsLoaded = createSelector(getRegionsState, fromRegions.getRegionsLoaded);

export const getRegionsFlatList = createSelector(getRegionsList, (regions) => Object.keys(regions).map(id => regions[id].name));

export const getSelectedRegion = createSelector(
  getRegionsFlatList,
  fromRoot.getRouterState,
  (entities, router): string => {
    return router.state && entities[router.state.params[regionIdParam] || router.state.parentParams[regionIdParam]];
  }
);
