import { createSelector } from '@ngrx/store';

import * as fromRoot from '../../../store/index';
import * as fromFeature from '../reducers/index';
import * as fromCategories from '../reducers/categories.reducer';

import { Categories } from 'resources/categories/categories';
import { categoryIdParam } from 'configs/paths.consts';

export const getCategoriesState = createSelector(
  fromFeature.getHomeState,
  (state: fromFeature.HomeState) => state.categories
);

export const getCategoriesList = createSelector(getCategoriesState, fromCategories.getCategoriesList);
export const getCategoriesLoaded = createSelector(getCategoriesState, fromCategories.getCategoriesLoaded);

export const getCategoriesFlatList = createSelector(getCategoriesList, (categories) => Object.keys(categories).map(id => categories[id].name));

export const getApplicableCategories = createSelector(
  getCategoriesList,
  (categories: Categories.List): Categories.FlatList => {
    return categories.filter(category => category.isApplicable).map(category => category.name);
  }
);

export const getSelectedCategoryName = createSelector(
  getCategoriesList,
  fromRoot.getRouterState,
  (entities, router): string => {
    return router.state && entities[router.state.params[categoryIdParam] || router.state.parentParams[categoryIdParam]].name;
  }
);
