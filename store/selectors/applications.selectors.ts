import { createSelector } from '@ngrx/store';

import * as fromRoot from '../../../store/index';
import * as fromFeature from '../reducers/index';
import * as fromApplications from '../reducers/applications.reducer';

import { Applications } from 'resources/applications/applications';
import { applicationIdParam } from 'configs/paths.consts';

export const getApplicationsState = createSelector(
  fromFeature.getHomeState,
  (state: fromFeature.HomeState) => state.applications
);

export const getApplicationsEntities = createSelector(getApplicationsState, fromApplications.getApplicationsEntities);
export const getAllApplications = createSelector(getApplicationsEntities, (entities) => Object.keys(entities).map(id => entities[id]));
export const getApplicationsLoaded = createSelector(getApplicationsState, fromApplications.getApplicationsLoaded);

export const getSelectedApplication = createSelector(
  getApplicationsEntities,
  fromRoot.getRouterState,
  (entities, router): Applications.Item => {
    return router.state && entities[router.state.params[applicationIdParam] || router.state.parentParams[applicationIdParam]];
  }
);
