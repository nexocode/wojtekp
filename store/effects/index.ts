import { ApplicationsEffect } from './applications.effect';
import { CategoriesEffect } from './categories.effect';
import { RegionsEffect } from './regions.effect';

export const effects: any[] = [
  ApplicationsEffect,
  CategoriesEffect,
  RegionsEffect,
];

export * from './applications.effect';
export * from './categories.effect';
export * from './regions.effect';
