import { Injectable } from '@angular/core';

import { Effect, Actions } from '@ngrx/effects';
import { map, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import * as categoriesActions from '../actions/categories.action';

import { Categories } from 'resources/categories/categories';
import { CategoriesService } from 'resources/categories/categories.service';

@Injectable()
export class CategoriesEffect {
  constructor(private actions$: Actions, private categoriesService: CategoriesService) {}

  @Effect()
  loadCategories$ = this.actions$
    .ofType(categoriesActions.LOAD_CATEGORIES)
    .pipe(switchMap(() => {
        return this.categoriesService.getCategories().pipe(
          map((categories: Categories.List) => new categoriesActions.LoadCategoriesSuccess(categories)),
          catchError(error => of(new categoriesActions.LoadCategoriesFail(error)))
        );
      })
    );

}
