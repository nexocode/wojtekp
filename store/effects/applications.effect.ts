import { Injectable } from '@angular/core';

import { Effect, Actions } from '@ngrx/effects';
import { map, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import * as applicationActions from '../actions/applications.action';

import { ApplicationsService } from 'resources/applications/applications.service';
import { Applications } from 'resources/applications/applications';

@Injectable()
export class ApplicationsEffect {
  constructor(private actions$: Actions, private applicationsService: ApplicationsService) {}

  @Effect()
  loadApplications$ = this.actions$
    .ofType(applicationActions.LOAD_APPLICATIONS)
    .pipe(switchMap(() => {
        return this.applicationsService.getApplications().pipe(
          map((applications: Applications.List) => new applicationActions.LoadApplicationsSuccess(applications)),
          catchError(error => of(new applicationActions.LoadApplicationsFail(error)))
        );
      })
    );

}
