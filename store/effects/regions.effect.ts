import { Injectable } from '@angular/core';

import { Effect, Actions } from '@ngrx/effects';
import { map, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import * as regionsActions from '../actions/regions.action';

import { Regions } from 'resources/regions/regions';
import { RegionsService } from 'resources/regions/regions.service';

@Injectable()
export class RegionsEffect {
  constructor(private actions$: Actions, private regionsService: RegionsService) {}

  @Effect()
  loadRegions$ = this.actions$
    .ofType(regionsActions.LOAD_REGIONS)
    .pipe(switchMap(() => {
        return this.regionsService.getRegions().pipe(
          map((regions: Regions.List) => new regionsActions.LoadRegionsSuccess(regions.sort((a, b) => a.name < b.name ? -1 : 1))),
          catchError(error => of(new regionsActions.LoadRegionsFail(error)))
        );
      })
    );

}
