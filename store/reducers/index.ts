import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromRoot from '../../../store';

import * as fromApplications from './applications.reducer';
import * as fromCategories from './categories.reducer';
import * as fromRegions from './regions.reducer';

export interface HomeState {
  applications: fromApplications.ApplicationsState;
  categories: fromCategories.CategoriesState;
  regions: fromRegions.RegionsState;
}

export interface AppState extends fromRoot.State {
  home: HomeState
}

export const reducers: ActionReducerMap<HomeState> = {
  applications: fromApplications.reducer,
  categories: fromCategories.reducer,
  regions: fromRegions.reducer,
};


export const getHomeState = createFeatureSelector<HomeState>('home');
export const getCurrentUser = fromRoot.getCurrentUser;
