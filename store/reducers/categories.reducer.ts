import * as fromCategories from '../actions/categories.action';
import { Categories } from 'resources/categories/categories';

export interface CategoriesState {
  list: Categories.List;
  loaded: boolean,
  loading: boolean
}

export const initialState: CategoriesState = {
  list: [],
  loaded: false,
  loading: false
};

export function reducer(
  state = initialState,
  action: fromCategories.CategoriesAction
): CategoriesState {
  switch (action.type) {
    case fromCategories.LOAD_CATEGORIES: {
      return {
        ...state,
        loading: true
      };
    }

    case fromCategories.LOAD_CATEGORIES_SUCCESS: {
      const list = action.payload;
      return {
        ...state,
        loading: false,
        loaded: true,
        list
      };
    }

    case fromCategories.LOAD_CATEGORIES_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      };
    }
  }

  return state;
}

export const getCategoriesList = (state: CategoriesState) => state.list;
export const getCategoriesLoaded = (state: CategoriesState) => state.loaded;
