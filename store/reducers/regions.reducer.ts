import * as fromRegions from '../actions/regions.action';
import { Regions } from 'resources/regions/regions';

export interface RegionsState {
  list: Regions.List;
  loaded: boolean,
  loading: boolean
}

export const initialState: RegionsState = {
  list: [],
  loaded: false,
  loading: false
};

export function reducer(
  state = initialState,
  action: fromRegions.RegionsAction
): RegionsState {
  switch (action.type) {
    case fromRegions.LOAD_REGIONS: {
      return {
        ...state,
        loading: true
      };
    }

    case fromRegions.LOAD_REGIONS_SUCCESS: {
      const list = action.payload;
      return {
        ...state,
        loading: false,
        loaded: true,
        list
      };
    }

    case fromRegions.LOAD_REGIONS_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      };
    }
  }

  return state;
}

export const getRegionsList = (state: RegionsState) => state.list;
export const getRegionsLoaded = (state: RegionsState) => state.loaded;
