import * as fromApplications from '../actions/applications.action';
import { Applications } from 'resources/applications/applications';

export interface ApplicationsState {
  entities: Applications.Entities;
  loaded: boolean,
  loading: boolean
}

export const initialState: ApplicationsState = {
  entities: {},
  loaded: false,
  loading: false
};

export function reducer(
  state = initialState,
  action: fromApplications.ApplicationsAction
): ApplicationsState {
  switch (action.type) {
    case fromApplications.LOAD_APPLICATIONS: {
      return {
        ...state,
        loading: true
      };
    }

    case fromApplications.LOAD_APPLICATIONS_SUCCESS: {
      const applications = action.payload;
      const entities = applications.reduce((entities: Applications.Entities, application) => {
          return {
            ...entities,
            [application.id]: application
          };
        },
        {
          ...state.entities
        }
      );
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      };
    }

    case fromApplications.LOAD_APPLICATIONS_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false
      };
    }
  }

  return state;
}

export const getApplicationsEntities = (state: ApplicationsState) => state.entities;
export const getApplicationsLoaded = (state: ApplicationsState) => state.loaded;
