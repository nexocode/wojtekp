import { TestBed, inject } from '@angular/core/testing';

import { ApplicationsFilterService } from './applications-filter.service';
import {
  ApplicationSummaryState, ApplicationSummaryState as summaryState
} from 'services/application-state/application-state';
import { AuthorizationService } from 'services/authorization/authorization.service';
import { Applications } from 'resources/applications/applications';
import { Users } from 'resources/users/users';

describe('ApplicationsFilterService', () => {

  let applicationsFilterService: ApplicationsFilterService;

  let authServiceValues = {
    isAdmin: false,
    isPoc: false,
    isRegionalLead: false,
    isProductInterviewer: false,
    isCommunityInterviewer: false,
  };

  const authServiceStub = {
    isAdmin: () => authServiceValues.isAdmin,
    isPoc: () => authServiceValues.isPoc,
    isRegionalLead: () => authServiceValues.isRegionalLead,
    isProductInterviewer: () => authServiceValues.isProductInterviewer,
    isCommunityInterviewer: () => authServiceValues.isCommunityInterviewer,
  };

  const resetAuthService = () => {
    authServiceValues = {
      isAdmin: false,
      isPoc: false,
      isRegionalLead: false,
      isProductInterviewer: false,
      isCommunityInterviewer: false,
    };
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ApplicationsFilterService,
        { provide: AuthorizationService, useValue: authServiceStub },
      ]
    });
  });

  beforeEach(inject([ApplicationsFilterService], (service: ApplicationsFilterService) => {
    applicationsFilterService = service;
  }));

  const applications = [{
    category: 'Angular',
    region: 'Poland',
    firstName: 'Foo',
    lastName: 'Bar',
    email: 'foo.bar@testable.com',
    processesState: {
      summary: ApplicationSummaryState.Active
    }
  }, {
    category: 'Android',
    region: 'Angola',
    firstName: 'Baz',
    lastName: 'Qux',
    email: 'baz.qux@nexo.zone',
    processesState: {
      summary: ApplicationSummaryState.Approved
    }
  }, {
    category: 'Firebase',
    region: 'Bolivia',
    firstName: 'Poo',
    lastName: 'Mill',
    email: 'poo.mill@gmail.com',
    processesState: {
      summary: ApplicationSummaryState.Closed
    }
  }, {
    category: 'Google Cloud Platorm',
    region: 'Spain',
    firstName: 'Zack',
    lastName: 'Wild',
    email: 'zack.wild@nexocode.com',
    processesState: {
      summary: ApplicationSummaryState.Inactive
    }
  }, {
    category: 'Web Technologies',
    region: 'Antigua and Barbuda',
    firstName: 'Witch',
    lastName: 'Zonk',
    email: 'witch.zonk@testable.com',
    processesState: {
      summary: Applications.Status.Pending
    }
  }];

  const userOfSingleCategory = <Users.Item>{
    email: 'foo.bar@gmail.com',
    roles: {
      poc: {
        isActive: true,
        categories: ['Android']
      }
    }
  };

  const userOfMultipleCategories = <Users.Item>{
    email: 'foo.bar@gmail.com',
    roles: {
      poc: {
        isActive: true,
        categories: ['Angular']
      },
      communityInterviewer: {
        isActive: true,
        categories: ['Android']
      },
      productInterviewer: {
        isActive: false,
        categories: ['Firebase']
      }
    }
  };

  const userOfSingleRegion = <Users.Item>{
    email: 'foo.bar@gmail.com',
    roles: {
      regionalLead: {
        isActive: true,
        regions: ['Poland']
      }
    }
  };

  const userOfMultipleRegions = <Users.Item>{
    email: 'foo.bar@gmail.com',
    roles: {
      regionalLead: {
        isActive: true,
        regions: ['Poland', 'Angola']
      }
    }
  };

  describe('filterByStatus', () => {

    it('should filter applications by "active" status', () => {
      const result = applicationsFilterService.filterByStatus(applications as Applications.List, Applications.Status.Active);
      expect(result).toEqual([applications[0], applications[4]] as any);
    });

    it('should filter applications by "all" status', () => {
      const result = applicationsFilterService.filterByStatus(applications as Applications.List, Applications.Status.All);
      expect(result).toEqual(applications as any);
    });

    it('should filter applications by "approved" status', () => {
      const result = applicationsFilterService.filterByStatus(applications as Applications.List, Applications.Status.Approved);
      expect(result).toEqual([applications[1]] as any);
    });

    it('should filter applications by "approved" status', () => {
      const result = applicationsFilterService.filterByStatus(applications as Applications.List, Applications.Status.Pending);
      expect(result).toEqual([applications[4]] as any);
    });

    it('should filter applications by "approved" status', () => {
      const result = applicationsFilterService.filterByStatus(applications as Applications.List, Applications.Status.Closed);
      expect(result).toEqual([applications[2]] as any);
    });
  });

  describe('filterByProperties', () => {

    let filterObject: Applications.Filters;

    const categories = ['Angular', 'Firebase'];

    const countries = ['Poland', 'Antigua and Barbuda'];

    const searchQuery = 'oo';

    it('should filter applications by categories', () => {
      filterObject = { categories };
      const result = applicationsFilterService.filterByProperties(applications as Applications.List, filterObject);
      expect(result).toEqual([applications[0], applications[2]] as any);
    });

    it('should filter applications by countries', () => {
      filterObject = { countries };
      const result = applicationsFilterService.filterByProperties(applications as Applications.List, filterObject);
      expect(result).toEqual([applications[0], applications[4]] as any);
    });

    it('should filter applications by query', () => {
      filterObject = { searchQuery };
      const result = applicationsFilterService.filterByProperties(applications as Applications.List, filterObject);
      expect(result).toEqual([applications[0], applications[2]] as any);
    });

    it('should filter applications by properies', () => {
      filterObject = { categories, countries, searchQuery };
      const result = applicationsFilterService.filterByProperties(applications as Applications.List, filterObject);
      expect(result).toEqual([applications[0]] as any);
    });
  });

  describe('countApplicationsProperties', () => {

    it('should return correct numbers', () => {
      const applicationsList = [
        { processesState: { summary: summaryState.Pending } },
        { overdue: 3, processesState: { summary: summaryState.Rejected } },
        { overdue: 0, processesState: { summary: summaryState.Pending } },
        { overdue: 3, processesState: { summary: summaryState.Pending } },
      ] as Applications.List;
      expect(applicationsFilterService.countApplicationsProperties(applicationsList).overdue).toBe(2);
      expect(applicationsFilterService.countApplicationsProperties(applicationsList).pendingActions).toBe(3)
    });


    it('should not return pending actions number for rejected applications if user is not admin', () => {
      const applicationsList = [
        { processesState: { summary: summaryState.Rejected } },
        { processesState: { summary: summaryState.Approved } }
      ] as Applications.List;
      expect(applicationsFilterService.countApplicationsProperties(applicationsList).pendingActions).toBe(0)
    });

    it('should not return overdue count if set as zero', () => {
      const applicationsList = [
        { overdue: 0, processesState: { summary: summaryState.Pending } },
      ] as Applications.List;
      expect(applicationsFilterService.countApplicationsProperties(applicationsList).overdue).toBe(0)
    });
  });

  describe('filterBySupervisedCategories', () => {

    it('should return applications by user category', () => {
      const result = applicationsFilterService.filterBySupervisedCategories(<Applications.List>applications, userOfSingleCategory);
      expect(result.length).toBe(1);
      expect(result).toEqual(<Applications.List>[applications[1]]);
    });

    it('should return applications by user categories', () => {
      const result = applicationsFilterService.filterBySupervisedCategories(<Applications.List>applications, userOfMultipleCategories);
      expect(result.length).toBe(1);
      expect(result).toEqual(<Applications.List>[applications[0]]);
    });

    it('should return empty list if user has no categories', () => {
      const result = applicationsFilterService.filterBySupervisedCategories(<Applications.List>applications, userOfSingleRegion);
      expect(result.length).toBe(0);
      expect(result).toEqual(<Applications.List>[]);
    });
  });

  describe('filterBySupervisedRegions', () => {

    it('should return applications by user region', () => {
      const result = applicationsFilterService.filterBySupervisedRegions(<Applications.List>applications, userOfSingleRegion);
      expect(result.length).toBe(1);
      expect(result).toEqual(<Applications.List>[applications[0]]);
    });

    it('should return applications by user regions', () => {
      const result = applicationsFilterService.filterBySupervisedRegions(<Applications.List>applications, userOfMultipleRegions);
      expect(result.length).toBe(2);
      expect(result).toEqual(<Applications.List>[applications[0], applications[1]]);
    });

    it('should return empty list if user has no regions', () => {
      const result = applicationsFilterService.filterBySupervisedRegions(<Applications.List>applications, userOfSingleCategory);
      expect(result.length).toBe(0);
      expect(result).toEqual(<Applications.List>[]);
    });
  });
});
