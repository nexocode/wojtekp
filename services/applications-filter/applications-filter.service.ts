import { Injectable } from '@angular/core';
import { groupBy, includes, pickBy } from 'lodash';

import { Applications } from 'resources/applications/applications';
import {
  ApplicationSummaryState,
  ApplicationProcessState,
  ApplicationSummaryState as summaryState, ApplicationState
} from 'services/application-state/application-state';
import { AuthorizationService } from 'services/authorization/authorization.service';
import { Users } from 'resources/users/users';
import { AttributeHelper } from 'utils/attribute-helper/attribute.helper';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsFilterService {

  constructor(private authorizationService: AuthorizationService) { }

  /**
   * Filters application by given status
   * @param {Applications.List} applications a list of applications to filter
   * @param {Applications.Status} status filter condition
   */
  filterByStatus(applications: Applications.List, status: Applications.Status): Applications.List {
    switch (status) {
      case Applications.Status.All:
        return applications;
      case Applications.Status.Active:
        return this.filter(applications, application =>
          application.processesState.summary === ApplicationSummaryState.Active ||
          application.processesState.summary === ApplicationSummaryState.Pending ||
          application.processesState.summary === ApplicationSummaryState.Rejected
        );
      case Applications.Status.Overdue:
        return this.filter(applications, application => application.overdue);
      case Applications.Status.Approved:
        return this.filter(applications, application =>
          application.processesState.summary === ApplicationSummaryState.Approved
        );
      case Applications.Status.Pending:
        return this.filter(applications, application =>
          application.processesState.summary === ApplicationSummaryState.Pending ||
          application.processesState.summary === ApplicationSummaryState.Rejected &&
          this.authorizationService.isAdmin() && AttributeHelper.isEscalationElapsed(application)
        );
      case Applications.Status.Closed:
        return this.filter(applications, application =>
          application.processesState.summary === ApplicationSummaryState.Closed
        );
    }
  }

  /**
   * Filters applications by given properties
   * @param {Applications.List} applications a list of applications to filter
   * @param {Applications.Filters} properties filter conditions
   * @return {Applications.List}
   */
  filterByProperties(applications: Applications.List, properties: Applications.Filters): Applications.List {
    return this.filter(applications, application =>
      (properties.countries && properties.countries.length ? properties.countries.indexOf(application.region) > -1 : true) &&
      (properties.categories && properties.categories.length ? properties.categories.indexOf(application.category) > -1 : true) &&
      (properties.searchQuery ? this.checkAgainstQuery(application, properties.searchQuery) : true)
    );
  }

  /**
   * Indicates whether application has pending process state
   * @param {ApplicationProcessState} applicationProcessState
   * @return {boolean}
   */
  isProcessStatePending(applicationProcessState: ApplicationProcessState): boolean {
    return applicationProcessState === ApplicationProcessState.Pending;
  }

  /**
   * Indicates whether application has rejected process state
   * @param {ApplicationProcessState} applicationProcessState
   * @return {boolean}
   */
  isProcessStateRejected(applicationProcessState: ApplicationProcessState): boolean {
    return applicationProcessState === ApplicationProcessState.Rejected;
  }

  private checkAgainstQuery(application: Applications.Item, query: string): boolean {
    const firstName = (application.firstName || '').toLocaleLowerCase();
    const lastName = (application.lastName || '').toLocaleLowerCase();
    const email = (application.email || '').toLocaleLowerCase();
    const field = firstName + lastName + email;
    if (!field) {
      return false;
    }
    query = query.toLocaleLowerCase();
    query = query.replace(/\s/g, '');
    return field.indexOf(query) !== -1;
  }

  private filter(applications: Applications.List, condition: Function): Applications.List {
    return applications.filter(condition.bind(this));
  }

  /**
   * Returns number of applications in specified process properties
   * @param {Applications.List} applications
   * @returns {Applications.ProcessPropertiesNumbers}
   *
   * @listens ApplicationsListPageComponent.countProcessProperties
   */
  countApplicationsProperties(applications: Applications.List): Applications.ProcessPropertiesNumbers {
    const numbers: Applications.ProcessPropertiesNumbers = {
      pendingActions: 0,
      overdue: 0,
    };
    applications.forEach(application => {
      if (
        application.processesState.summary === summaryState.Rejected &&
        this.authorizationService.isAdmin() && AttributeHelper.isEscalationElapsed(application)
        || application.processesState.summary === summaryState.Pending
      ) {
        numbers.pendingActions++;
      }
      if (application.overdue) {
        numbers.overdue++;
      }
    });
    return numbers;
  }

  /**
   * Returns applications matching categories of given user
   * @param {Applications.List} applications
   * @param {Users.Item} user
   *
   * @returns {Applications.List}
   */
  filterBySupervisedCategories(applications: Applications.List, user: Users.Item): Applications.List {
    return this.filterByUserAttributes(applications, AttributeHelper.getSupervisedCategories(user), 'category');
  }

  /**
   * Returns applications matching regions of given user
   * @param applications
   * @param {Users.Item} user
   *
   * @returns {Applications.List}
   */
  filterBySupervisedRegions(applications: Applications.List, user: Users.Item): Applications.List {
    return this.filterByUserAttributes(applications, AttributeHelper.getSupervisedRegions(user), 'region');
  }

  /**
   * Returns applications given user attributes
   * @param applications
   * @param attributes
   * @param attribute
   *
   * @returns {Applications.List}
   */
  private filterByUserAttributes(applications: Applications.List, attributes: string[], attribute: 'region' | 'category'): Applications.List {
    return this.filter(applications, application => includes(attributes, application[attribute]));
  }

  /**
   * Group applications by given Attribute
   *
   * @param {Applications.List} applications
   * @param {string} attribute
   *
   * @returns {Applications.Dictionary}
   * @listens DiversityTableStatsComponent
   */
  groupApplicationsByAttribute(applications: Applications.List, attribute: string): Applications.Dictionary {
    return pickBy(groupBy(applications, attribute), (list, key) => key !== 'undefined');
  }

  filterByState(applications: Applications.List, state: keyof ApplicationState): Applications.List {
    return applications.filter(application => application.processesState[state]);
  }
}
